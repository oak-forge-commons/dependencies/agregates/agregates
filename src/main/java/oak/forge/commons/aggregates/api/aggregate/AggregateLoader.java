package oak.forge.commons.aggregates.api.aggregate;

import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;


public interface AggregateLoader {

    <S extends AggregateState, A extends AggregateRoot<S>> A load(Command command, EventStore eventStore, Context context);

    <S extends AggregateState> void unload(AggregateRoot<S> aggregateRoot);
}
