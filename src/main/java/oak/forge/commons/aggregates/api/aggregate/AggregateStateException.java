package oak.forge.commons.aggregates.api.aggregate;

public class AggregateStateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AggregateStateException(String message) {
		super(message);
	}
}
