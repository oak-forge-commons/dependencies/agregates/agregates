package oak.forge.commons.aggregates.api.command;

import oak.forge.commons.data.message.Command;

@FunctionalInterface
public interface CommandPreparer<C extends Command> {

	void prepare(C command);
}
