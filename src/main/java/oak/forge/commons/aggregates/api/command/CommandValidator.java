package oak.forge.commons.aggregates.api.command;

import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.DomainError;

import java.util.List;

public interface CommandValidator<C extends Command, S extends AggregateState> {

	List<DomainError> validate(C command, S state);
}
