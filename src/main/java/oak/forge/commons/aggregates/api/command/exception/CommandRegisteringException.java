package oak.forge.commons.aggregates.api.command.exception;

public class CommandRegisteringException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CommandRegisteringException(String message) {
		super(message);
	}
}
