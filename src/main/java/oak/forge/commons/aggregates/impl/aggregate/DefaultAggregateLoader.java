package oak.forge.commons.aggregates.impl.aggregate;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.aggregates.api.aggregate.AggregateRoot;
import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.eventsourcing.api.event.EventStreamValidator;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;

import java.util.List;
import java.util.UUID;

public class DefaultAggregateLoader implements AggregateLoader {

    private final EventStreamValidator eventStreamValidator;

    public DefaultAggregateLoader(EventStreamValidator eventStreamValidator) {
        this.eventStreamValidator = eventStreamValidator;
    }

    @Override
    public <S extends AggregateState, A extends AggregateRoot<S>> A load(Command command, EventStore eventStore, Context context) {
        AggregateRoot<AggregateState> aggregateRoot = context.getAggregateRoot(command.getName());

        aggregateRoot.prepareCommand(command);
        UUID aggregateId = command.getAggregateId();
        aggregateRoot.init(aggregateId);
        if (aggregateId != null) {
            List<Event> eventStream = eventStore.getAggregateEventStream(aggregateId);
            eventStreamValidator.validateAggregateId(eventStream).
                    ifPresent(uuid -> aggregateRoot.handleEvents(eventStream));
        }
        return (A) aggregateRoot;
    }


    @Override
    public <S extends AggregateState> void unload(AggregateRoot<S> aggregateRoot) {
        if (aggregateRoot != null) {
            aggregateRoot.detachState();
        }
    }
}
