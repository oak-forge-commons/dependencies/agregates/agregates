package oak.forge.commons.aggregates.testutils;

import oak.forge.commons.data.message.Event;

import java.util.Iterator;
import java.util.List;

public class Events {

	private final List<Event> events;
	private final Iterator<Event> iterator;

	private Events(List<Event> events) {
		this.events = events;
		this.iterator = events.iterator();
	}

	public static Events of(List<Event> events) {
		return new Events(events);
	}

	public List<Event> source() {
		return events;
	}

	public <E extends Event> E next() {
		return (E) iterator.next();
	}

	public Events skip(int n) {
		while (n-- > 0) next();
		return this;
	}
}
