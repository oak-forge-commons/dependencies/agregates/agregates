package oak.forge.commons.aggregates.impl.aggregate;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.aggregates.test.RawTestCommand;
import oak.forge.commons.aggregates.test.TestAggregate;
import oak.forge.commons.aggregates.test.TestCommand;
import oak.forge.commons.aggregates.test.TestEvent;
import oak.forge.commons.eventsourcing.api.event.EventStreamValidator;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Arrays.asList;
import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class DefaultAggregateLoaderTest {

	private static final String NAME = "name";
	private static final Instant timestamp = Instant.now();

	private final EventStreamValidator eventStreamValidator = mock(EventStreamValidator.class);
	private final DefaultAggregateLoader factory = new DefaultAggregateLoader(eventStreamValidator);

	private final Context context = mock(Context.class);
	private final EventStore eventStore = mock(EventStore.class);
	private final AggregateLoader aggregateLoader = mock(AggregateLoader.class);
	private final MessageBus messageBus = mock(MessageBus.class);

	private TestAggregate aggregate;


	@Before
	public void init(){
		aggregate = new TestAggregate(messageBus,eventStore,aggregateLoader,context);
	}

	@Test
	public void should_load_aggregate_for_command_with_aggregate_id() {
		// given
		UUID id = UUID.randomUUID();
		TestCommand command = new TestCommand(NAME, id);
		when(context.getAggregateRoot(NAME)).thenReturn(aggregate);
		List<Event> events = asList(
				new TestEvent(id, 1),
				new TestEvent(id, 2),
				new TestEvent(id, 3)
		);
		when(eventStore.getAggregateEventStream(id)).thenReturn(events);
		when(eventStreamValidator.validateAggregateId(events)).thenReturn(Optional.of(id));

		// when
		AbstractAggregateRoot<AggregateState> root = factory.load(command, eventStore, context);

		// then
		assertThat(root).isInstanceOf(TestAggregate.class);
		assertThat(root.getState()).isNotNull();
	}

	@Test
	public void should_unload_aggregate() {
		// given
		aggregate.attachState(new TestAggregate.State(randomUUID()));

		// when
		factory.unload(aggregate);

		// then
		Assertions.assertThat(aggregate.getState()).isNull();
	}

	@Test
	public void should_load_aggregate_for_command_without_aggregate_id() {
		// given
		TestCommand command = new TestCommand();
		command.withName(NAME);
		when(context.getAggregateRoot(NAME)).thenReturn(aggregate);

		// when
		AbstractAggregateRoot<AggregateState> root = factory.load(command, eventStore, context);

		// then
		assertThat(root).isInstanceOf(TestAggregate.class);
		assertThat(root.getState()).isNotNull();
		assertThat(root.getState().getAggregateId()).isNull();
	}

	@Test
	public void should_run_command_preparer() {
		// given
		RawTestCommand command = new RawTestCommand();
		command.withName(NAME);
		when(context.getAggregateRoot(NAME)).thenReturn(aggregate);

		// when
		factory.load(command, eventStore, context);

		// then
		assertThat(command.isPrepared()).isTrue();
	}

}
