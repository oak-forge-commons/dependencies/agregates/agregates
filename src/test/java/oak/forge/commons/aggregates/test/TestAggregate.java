package oak.forge.commons.aggregates.test;

import oak.forge.commons.aggregates.api.aggregate.AggregateLoader;
import oak.forge.commons.bus.MessageBus;
import oak.forge.commons.context.Context;
import oak.forge.commons.data.aggregate.AggregateState;
import oak.forge.commons.data.message.Command;
import oak.forge.commons.data.message.Event;
import oak.forge.commons.data.version.Version;
import oak.forge.commons.eventsourcing.api.eventstore.EventStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oak.forge.commons.aggregates.impl.aggregate.AbstractAggregateRoot;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static oak.forge.commons.aggregates.test.TestEvent.CREATOR_ID;


public class TestAggregate extends AbstractAggregateRoot<TestAggregate.State> {

    private static final Logger LOG = LoggerFactory.getLogger(TestAggregate.class);
    private static final Instant timestamp = Instant.now();

    public static class State extends AggregateState {
        private static final long serialVersionUID = 1L;

        public String string;

        public State(UUID aggregateId) {
            super(aggregateId);
        }

        public State(UUID aggregateId, Version version) {
            super(aggregateId, version);
        }
    }

    private UUID correlationId;

    public TestAggregate(MessageBus messageBus, EventStore eventStore, AggregateLoader aggregateLoader, Context context) {
        super(messageBus,eventStore, aggregateLoader, context);
        registerCommandPreparer(RawTestCommand.class, this::prepare);
        registerCommandHandler(TestCommand.class, this::handle);
        registerCommandHandler(GenerateMultipleEvents.class, this::handle);
        registerCommandHandler(OtherTestCommand.class, this::handle);
        registerCommandHandler(UpdateCommonProperties.class, this::handle);
        registerCommandHandler(ThrowingTestCommand.class, this::handle);

		registerEventHandler(TestEvent.class, this::handleTestEvent);
    }

    @Override
    public State initState(UUID aggregateId) {
        return new State(aggregateId);
    }

    public void simulateProcess(UUID correlationId) {
        this.correlationId = correlationId;
    }

    private TestEvent getTestEvent(Command command) {
        return new TestEvent(command.getAggregateId(), CREATOR_ID);
    }

    private void prepare(RawTestCommand command) {
        command.prepare();
    }

    private List<Event> handle(TestCommand command, State state) {
        LOG.debug("handling [{}] with state [{}]", command, state);
        TestEvent event = getTestEvent(command);
        if (correlationId != null) {
            event.correlatingTo(correlationId);
        }
        return singletonList(event);
    }

    private List<Event> handle(OtherTestCommand command, State state) {
        return emptyList();
    }

    private List<Event> handle(GenerateMultipleEvents command, State state) {
        return asList(
                getTestEvent(command),
                getTestEvent(command),
                getTestEvent(command)
        );
    }

	private void handleTestEvent(TestEvent event, State state) {
        LOG.debug("handling TestEvent");
        getState().string = event.getAggregateId().toString();
    }

    private List<Event> handle(UpdateCommonProperties command, State state) {
        return singletonList(new TestEvent());
    }

    private List<Event> handle(ThrowingTestCommand command, State state) {
        throw new StrangeException();
    }
}
