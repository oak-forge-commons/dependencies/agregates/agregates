package oak.forge.commons.aggregates.test;

import oak.forge.commons.data.message.Command;

import java.time.Instant;
import java.util.UUID;

public class TestCommand extends Command {

    private static final long serialVersionUID = -2228432523016590578L;
    public String c;

    public TestCommand() {
    }

    public TestCommand(String c) {
        this.c = c;
    }

    public TestCommand(String name, UUID id) {
        super(id);
        withName(name);
    }

}
