package oak.forge.commons.aggregates.test;

import oak.forge.commons.data.message.Command;

import java.time.Instant;
import java.util.UUID;

public class ThrowingTestCommand extends Command {

    private static final long serialVersionUID = -2228432523016590578L;

    public String c;

    public ThrowingTestCommand() {
    }

    public ThrowingTestCommand(String c) {
        this.c = c;
    }

    public ThrowingTestCommand(String name, UUID id) {
        super(id);
        withName(name);
    }

}
