package oak.forge.commons.aggregates.test;

import oak.forge.commons.data.message.Command;

import java.time.Instant;
import java.util.UUID;

public class UpdateCommonProperties extends Command {

    private static final long serialVersionUID = 1L;

    public UpdateCommonProperties() {
    }

    public UpdateCommonProperties(UUID aggregateId, UUID creatorId) {
        super(aggregateId, creatorId);
    }
}
