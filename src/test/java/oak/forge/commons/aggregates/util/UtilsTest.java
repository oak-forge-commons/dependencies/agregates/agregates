package oak.forge.commons.aggregates.util;

import oak.forge.commons.aggregates.test.TestGeneric;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class UtilsTest {

	@Test
	public void should_check_string() {
		// then
		assertThat(Utils.isEmpty((String) null)).isTrue();
		assertThat(Utils.isEmpty("")).isTrue();
		assertThat(Utils.isEmpty("x")).isFalse();
		assertThat(Utils.isNotEmpty("")).isFalse();
		assertThat(Utils.isNotEmpty("x")).isTrue();
	}

	@Test
	public void should_check_string_array() {
		// then
		assertThat(Utils.isEmpty((String[]) null)).isTrue();
		assertThat(Utils.isEmpty(new String[]{})).isTrue();
		assertThat(Utils.isEmpty(new String[]{null})).isTrue();
		assertThat(Utils.isEmpty(new String[]{""})).isTrue();
		assertThat(Utils.isEmpty(new String[]{"x"})).isFalse();
	}

	@Test
	public void should_check_arguments() {
		// given
		Integer v1 = null;
		Integer v2 = 17;
		String errorMessage = "v1 cannot be null";

		// then
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> Utils.checkArgument(v1 != null, errorMessage))
				.withMessageContaining(errorMessage);

		Utils.checkArgument(v2 != null, ""); // no exception is thrown
	}

	//replace TestGeneric by another generic class if it appears in any module
	private static class E1 extends TestGeneric<Integer> {
		private static final long serialVersionUID = -484388573167499054L;
	}

	@Test
	public void should_get_generic_argument() {
		// then
		assertThat(Utils.getGenericArgumentType(E1.class, 0)).isEqualTo(Integer.class);
	}

	@Test
	public void should_find_first_not_null() {
		// when
		String first = Utils.findFirstOrThrow(() -> new RuntimeException("not found"),
				() -> null,
				() -> "first"
		);

		// then
		assertThat(first).isEqualTo("first");
	}

	@Test
	public void should_throw_for_missing_not_null_argument() {
		// then
		assertThatExceptionOfType(RuntimeException.class)
				.isThrownBy(() -> Utils.findFirstOrThrow(() -> new RuntimeException("not found"),
						() -> null
				))
				.withMessage("not found");
	}
}